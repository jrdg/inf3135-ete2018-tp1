.PHONY: clean html check
exec=tp1
cc=gcc
cflag=-Wall --std=c11
test=test

all: $(exec)

tp1: tp1.o

%: %.o
	$(cc) $(cflag) -o $@ $<

%.o: %.c
	$(cc) $(cflag) -c $<

clean:
	rm -f $(exec) *.o *.swp *.html

check:
	bats $(test).bats

html:
	pandoc README.md -f markdown -t html -s  -o README.html

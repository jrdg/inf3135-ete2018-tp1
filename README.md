# Travail pratique 1 : Génération d'une scène ASCII

## Description

Ce travail pratique a ete concu dans le cadre du cours de construction et
maintenant de logiciel portant le signe INF-3135 donne par le professeur
Alexandre Blondin-Masse de l'universite de Quebec a Montreal.
Ce travail avait pour but de familiariser l'etudiant au fondation
du **langage de programation C** , du logiciel **Git** , de faire la compilation
a l'aide d'un fichier `Makefile` ainsi que de faire la conversion d'un fichier
format **markdown** a **html** avec **pandoc**  
  
Le programe lit le fichier d'entree standard `STDIN` pour creer une scene
**ASCII** qui sera ensuite envoyer sur la sorti standard `STDOUT`
ou bien si les informations sont invalide un message  d'erreur approprie sera
envoye a partir des informations recupere.

## Auteur

**Prenom** : Jordan  
**Nom** :  Gauthier  
**Code permanent** : GAUJ25089201

## Fonctionnement

#### Compilation

Tout d'abord il faut savoir que le code source du projet reside dans le fichier `tp1.c`.

Ensuite nous pouvons proceder de deux maniere differente pour compiler le projet.

- Methode 1 conventionelle : Nous devrons taper les commandes de compilation a la main.
- Methode 2 : Taper les commande defini dans le fichier `Makefile`.

###### Methode 1 :

Pour la methode 1 nous devrons proceder dans l'ordre au commande suivante dans le terminal.
1. Premierement tapez `gcc -c tp1.c ` cette commande generera un fichier object tp1.o qui servira a generer l'executable.
1. Ensuite tapez `gcc -o tp1  tp1.o` cette commande generera l'executable `tp1`. 
1. Pour lancer l'executable, vous devez simplement tapez `./tp1 < <Nom du fichier a introduire dans STDIN>`.
**A noter que chaque modification du code necessitera de taper tous ces commande a nouveau.**

###### Methode 2 :

1. tapez la commande `make clean` qui aura pour but de supprimer la compilation courrante.
1. tapez la commande `make` qui generera le nouveau executable ainsi que le fichier tp1.o.
1. Pour lancer l'executable, vous devez simplement tapez `./tp1 < <Nom du fichier a introduire dans STDIN>`.  
**A noter que chaque modification du code necessitrera de traper tous ces commande a nouveau.**

#### Entree/Formattage

Le fichier que vous utiliserez comme entree dans `STDIN` ce doit de respecter certains criteres predefinies.

**Les espaces et les lignes vides (ligne compose uniquement d'espace et\ou d'un saut de ligne sont ignore)**

1. premiere ligne : La premiere ligne lue doit respecter le format suivant : `<row> <col>`
2. Chaque ligne additionnelle doit respecter le format associer a sa forme:
    - rectangle : `r <row> <col> <height> <width> <symbol>`.
    - croix : `+ <row> <col> <symbol>`
    - disque :  `d <row> <col> <rayon> <symbol>`

**Si les formats specifies ne sont pas respecte, un message d'erreur s'affichera.**

**A noter que pour chaques formes les valeurs affectees a `<row>` et `<col>` peuvent etres negatives**
     
## Tests

Pour lancer la suite de tests, vous devez simplement taper la commande `make check` dans le terminal 
en s'assurant biensur d'avoir d'abord compiler le projet avec la commande `make`.

Nombre de test reussi : **20/20**

## Dépendances

- [GCC](https://gcc.gnu.org)
- [Bats](https://github.com/sstephenson/bats)
- [pandoc](https://pandoc.org)

## Références

- [Documentation de c sur cplusplus](http://www.cplusplus.com/reference/clibrary/)
- [GCC](https://gcc.gnu.org) 
- [pandoc](https://pandoc.org)

## État du projet

Indiquez toutes les tâches qui ont été complétés en insérant un `X` entre les
crochets. Si une tâche n'a pas été complétée, expliquez pourquoi (lors de la
remise, vous pouvez supprimer les phrases précédentes).

* [X] Le nom du dépôt GitLab est exactement `inf3135-ete2018-tp1` (Pénalité de
  **50%**).
* [X] L'URL du dépôt GitLab est exactement (remplacer `utilisateur` par votre
  nom identifiant GitLab) `https://gitlab.com/utilisateur/inf3135-ete2018-tp1`
  (Pénalité de **50%**).
* [X] L'utilisateur `ablondin` a accès au projet en mode *Developer* (Pénalité
  de **50%**).
* [X] Le dépôt GitLab est un **clone** du [gabarit
  fourni](https://gitlab.com/ablondin/inf3135-ete2018-tp1) (Pénalité de
  **50%**).
* [X] Le dépôt GitLab est privé (Pénalité de **50%**).
* [X] Le dépôt contient au moins un fichier `.gitignore`.
* [X] Le fichier Makefile permet de compiler le projet lorsqu'on entre `make`.
  Il supporte les cibles `html`, `check` et `clean`. Il 
* [X] Le nombre de tests qui réussissent/échouent avec la `make check` est
  indiqué.
* [X] Les sections incomplètes du fichier `README.md` ont été complétées.
* [X] L'en-tête du fichier est documentée.
* [X] L'en-tête des prototypes est documentée (*docstring*).
* [X] Le programme ne contient pas de valeurs magiques.

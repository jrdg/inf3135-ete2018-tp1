#!/usr/bin/env bats

# General behavior

@test "Empty 4x8 scene" {
  ./tp1 < examples/empty.scn > $BATS_TMPDIR/empty.out
  run diff -w $BATS_TMPDIR/empty.out output/empty.out
  [ "$output" = "" ]
}

@test "More complex example" {
  ./tp1 < examples/more_complex.scn > $BATS_TMPDIR/more_complex.out
  run diff -w $BATS_TMPDIR/more_complex.out output/more_complex.out
  [ "$output" = "" ]
}

@test "Scene with just one rectangle" {
  ./tp1 < examples/just_rectangle.scn > $BATS_TMPDIR/just_rectangle.out
  run diff -w $BATS_TMPDIR/just_rectangle.out output/just_rectangle.out
  [ "$output" = "" ]
}

@test "Scene with just one plus object" {
  ./tp1 < examples/just_plus.scn > $BATS_TMPDIR/just_plus.out
  run diff -w $BATS_TMPDIR/just_plus.out output/just_plus.out
  [ "$output" = "" ]
}

@test "Scene with just one disk" {
  ./tp1 < examples/just_disk.scn > $BATS_TMPDIR/just_disk.out
  run diff -w $BATS_TMPDIR/just_disk.out output/just_disk.out
  [ "$output" = "" ]
}

@test "Valid scene with all objects" {
  ./tp1 < examples/all_objects.scn > $BATS_TMPDIR/all_objects.out
  run diff -w $BATS_TMPDIR/all_objects.out output/all_objects.out
  [ "$output" = "" ]
}

@test "Multiple overlaps" {
  ./tp1 < examples/overlap.scn > $BATS_TMPDIR/overlap.out
  run diff -w $BATS_TMPDIR/overlap.out output/overlap.out
  [ "$output" = "" ]
}

@test "Partially out of scene" {
  ./tp1 < examples/part_out_of_scene.scn > $BATS_TMPDIR/part_out_of_scene.out
  run diff -w $BATS_TMPDIR/part_out_of_scene.out output/part_out_of_scene.out
  [ "$output" = "" ]
}

@test "Completely out of scene" {
  ./tp1 < examples/out_of_scene.scn > $BATS_TMPDIR/out_of_scene.out
  run diff -w $BATS_TMPDIR/out_of_scene.out output/out_of_scene.out
  [ "$output" = "" ]
}

# Status and errors

@test "Scene with all objects" {
  run ./tp1 < examples/all_objects.scn
  [ "$status" -eq 0 ]
}

@test "Large scene" {
  run ./tp1 < examples/large.scn
  [ "$status" -eq 0 ]
}

@test "Wrong number of arguments for dimensions" {
  run ./tp1 < examples/wrong_args_number_dimensions.scn
  [ "$status" -eq 1 ]
}

@test "Wrong type of arguments for dimensions" {
  run ./tp1 < examples/wrong_type_dimensions.scn
  [ "$status" -eq 1 ]
}

@test "Too many lines in scene" {
  run ./tp1 < examples/too_many_lines.scn
  [ "$status" -eq 1 ]
  [ "$output" = "Error: The number of lines must be between 1 and 20." ]
}

@test "Too many columns in scene" {
  run ./tp1 < examples/too_many_columns.scn
  [ "$status" -eq 1 ]
  [ "$output" = "Error: The number of columns must be between 1 and 40." ]
}

@test "Wrong object type" {
  run ./tp1 < examples/wrong_object.scn
  [ "$status" -eq 2 ]
  [ "$output" = "Error: Unrecognized object type: 'a'." ]
}

@test "Wrong rectangle" {
  run ./tp1 < examples/wrong_rectangle.scn
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error: invalid rectangle object." ]
  [ "${lines[1]}" = "Expected: r <i> <j> <h> <w> <c>, where" ]
  [ "${lines[2]}" = "  <i> is the line number" ]
  [ "${lines[3]}" = "  <j> is the column number" ]
  [ "${lines[4]}" = "  <h> is the height of the rectangle" ]
  [ "${lines[5]}" = "  <w> is the width of the rectangle" ]
  [ "${lines[6]}" = "  <c> is a char denoting the object." ]
}

@test "Wrong plus" {
  run ./tp1 < examples/wrong_plus.scn
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error: invalid '+' object." ]
  [ "${lines[1]}" = "Expected: + <i> <j> <c>, where" ]
  [ "${lines[2]}" = "  <i> is the line number of the crossing" ]
  [ "${lines[3]}" = "  <j> is the column number of the crossing" ]
  [ "${lines[4]}" = "  <c> is a char denoting the object." ]
}

@test "Wrong disk" {
  run ./tp1 < examples/wrong_disk.scn
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error: invalid disk object." ]
  [ "${lines[1]}" = "Expected: d <i> <j> <r> <c>, where" ]
  [ "${lines[2]}" = "  <i> is the line number of the center of the disk" ]
  [ "${lines[3]}" = "  <j> is the column number of the center of the disk" ]
  [ "${lines[4]}" = "  <r> is the radius of the disk" ]
  [ "${lines[5]}" = "  <c> is a char denoting the object." ]
}

@test "Negative radius" {
  run ./tp1 < examples/negative_radius.scn
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error: invalid disk object." ]
  [ "${lines[1]}" = "Expected: d <i> <j> <r> <c>, where" ]
  [ "${lines[2]}" = "  <i> is the line number of the center of the disk" ]
  [ "${lines[3]}" = "  <j> is the column number of the center of the disk" ]
  [ "${lines[4]}" = "  <r> is the radius of the disk" ]
  [ "${lines[5]}" = "  <c> is a char denoting the object." ]
}

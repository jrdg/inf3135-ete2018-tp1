/**
 * TP1 du cours de construction et maintenance de logiciel
 *
 * Le programe lit le fichier d'entree standard STDIN pour creer une scene
 * ASCII qui sera ensuite envoyer sur la sorti standard STDOUT
 * ou bien si les informations sont invalide un message  d'erreur approprie sera
 * envoye a partir des informations recupere.
 *
 * Pour utiliser le programme il suffit de creer un fichier avec
 * l'extension .scn puis d'inserer des lignes decrivant la scene.
 * 
 * exemple de fichier .scn decrivant une scene comportant un rectangle
 * avec les propriete suivante : coordonne (ligne,col)(3,5) dimension
 * (hauteur,largeur)(2,5) et qui sera demontrer par la lettre 'H' sur 
 * une scene de 12 lignes et 28 colones :
 *
 * INPUT:
 *
 * 12 28
 * r 3 5 2 5 H
 *
 * OUTPUT :
 *
 * ............................
 * ............................
 * ............................
 * .....HHHHH..................
 * .....HHHHH..................
 * ............................
 * ............................
 * ............................
 * ............................
 * ............................
 * ............................
 * ............................
 *
 *
 * Pour plus d'information sur l'utilisation du programme veuillez consulter
 * la page dedie du programme : https://gitlab.com/jrdg/inf3135-ete2018-tp1
 *
 * @sigle INF-3135
 * @author Jordan Gauthier
 * @version 1.0
 * @date 7 juin 2018
 */

#include <stdio.h>
#include <math.h>
#include <string.h>

/**
 * grosseur du buffer 
 * 
 * Pourquoi une grosseur de 100 : 
 *
 * Le buffer necessite une grandeur > 4 a cause que nous devons
 * imprimer la sequence de chars d'un objet non reconnue.
 */
#define BUFFER_SIZE 100

//grosseur du buffer qui contiendra l'erreur dans le cas d'un objet non reconnue.
#define ERR_BUFFER_SIZE 100

//nombre de caractere qu'on doit ajouter au tableau err
#define NB_CHARS_ADD_ERR 3

//exposant utilise dans le calcule du disque
#define EXP 2

//Variables limit pour la scene
#define MAX_ROW 20
#define MAX_COL 41
#define MIN_ROW 1
#define MIN_COL 1

//vairables limite pour un disque
#define D_TOKEN_NEG 4 
#define MAX_TOKEN_D 5 

//variable limite pour la premiere ligne
#define MAX_TOKEN_FIRST_LINE 2

//Variables limites pour la forme rectangle
#define MAX_TOKEN_R 6
#define R_TOKEN_NEG 3

//variables limites pour une croix
#define MAX_TOKEN_P 4

//Lettre representant les formes distinctes.
#define TYPE_R 'r'
#define TYPE_D 'd'
#define TYPE_P '+'

//Numero de token
#define ROW 2
#define COL 3
#define WIDTH 5
#define HEIGHT 4
#define RAYON 4

//Codes d'erreurs
#define ERR_INVALID_DIMENSION 1
#define ERR_INVALID_OBJECT 2

//Message d'erreur.
#define ERR_FL_DIM "Error: The two first values must indicate the dimensions of the scene."
#define ERR_FL_ROW "Error: The number of lines must be between 1 and 20."
#define ERR_FL_COL "Error: The number of columns must be between 1 and 40."
#define ERR_TYPE_OBJECT "Error: Unrecognized object type: '"
#define ERR_RECTANGLE "Error: invalid rectangle object.\n\
Expected: r <i> <j> <h> <w> <c>, where\n\
  <i> is the line number\n\
  <j> is the column number\n\
  <h> is the height of the rectangle\n\
  <w> is the width of the rectangle\n\
  <c> is a char denoting the object."

#define ERR_PLUS "Error: invalid '+' object.\n\
Expected: + <i> <j> <c>, where\n\
  <i> is the line number of the crossing\n\
  <j> is the column number of the crossing\n\
  <c> is a char denoting the object."

#define ERR_DISK "Error: invalid disk object.\n\
Expected: d <i> <j> <r> <c>, where\n\
  <i> is the line number of the center of the disk\n\
  <j> is the column number of the center of the disk\n\
  <r> is the radius of the disk\n\
  <c> is a char denoting the object."

/**
 * Structure representant la scene a afficher
 *
 * @attribut rows nombre de lignes que contient la scene
 * @attribut cols nombre de colonnes que contient la scene
 */
struct Scene{
    unsigned int rows;
    unsigned int cols;
    char scene[MAX_ROW][MAX_COL];
};

/**
 * Structure representant l'etat du progame, si celui-ci rencontre un erreur ou non.
 *
 * @attribut msgerr pointeur sur le premier char d'une chaine de caractere representant un message d'erreur
 * @attribut err_code code d'erreur associe a un erreur rencontrer pendent l'execution.
 */
struct Error{
    const char *msgerr;
    unsigned int err_code;
};

/**
 * Structure representant une forme de rectangle dans la scene.
 *
 * @attribut row indice de la ligne  duquelle le rectangle commencera
 * @attribut col indice de la colonne duquelle le rectangle commencera
 * @attribut height hauteur du rectangle
 * @attribut width largeur du rectangle
 * @attribut symbol symbol qu'aura chaque cellule de la scene representant le rectangle
 */
struct Rectangle{
    signed int row;
    signed int col;
    unsigned int height;
    unsigned int width;
    char symbol;
};

/**
 * Structure representant une forme de croix dans la scene.
 *
 * @attribut row indice de la ligne  duquelle la croix commencera
 * @attribut col indice de la colonne duquelle la croix  commencera
 * @attribut symbol symbol qu'aura chaque cellule de la scene representant la croix.
 */
struct Plus{
    signed int row;
    signed int col;
    char symbol;
};

/**
 * Structure representant une forme de disque dans la scene.
 *
 * @attribut row indice de la ligne  duquelle le disque commencera
 * @attribut col indice de la colonne duquelle le disque commencera
 * @attribut rayon le rayon qu'a le disque
 * @attribut symbol symbol qu'aura chaque cellule de la scene representant le disque.
 */
struct Disk{
    signed int row;
    signed int col;
    unsigned int rayon;
    char symbol;
};

/**
 * <p>
 *     va retourner le nombre de caractere que compose cette chaine en soustrayant si il y a lieu le premier caractere si
 *     celui-ci est egal a '-' ou '+'.
 * </p>
 *
 * <p>Exemple :</p>
 * <p> si str = "-123" la methode retournera 3</p>
 * <p> si str = "123" la methode retournera 3</p>
 *
 * @param str  Un pointeur sur le premier caractere d'une chaine de caractere.
 * @return le nombre de caractere qui compose str.
 */
unsigned int len_int(const char * const str);

/**
 * <p>transforme une chaine de caractere en signed int.</p>
 *
 * <p>***A NOTER***</p>
 * <p>Cette methode ne teste pas si la chaine peu etre convertie en int. De ce fait vous devriez preferablement
 * utiliser la methode is_int() avant d'employer celle-ci.</p>
 *
 * @param str pointeur vers le premier char d'une chaine de caractere.
 * @return Un signed int representent la chaine str
 */
signed int to_int(const char * const str);

/**
 * <p>
 *     Retourne 0 si str contient un caractere qui ne peu pas etre convertie en son egal en nombre.
 *     Retourne 1 si tous les caractere peuvent etre representer par un nombre de 0-9.
 *     A noter que les caractere '-' et '+' si situer a l'indice 0 de str sont ignore et donc ne retournera pas 0.
 * </p>
 *
 * @param str pointeur sur le premier caractere d'une chaine
 * @return 1 si str peut etre convertit en nombre 0 sinon.
 */ 
unsigned int is_int(const char * const str);

/**
 * <p>
 *     initie tous les cellule du tableau (*scene).scene entre les indices [0][0]-[row-1][col-1] (A noter que row et col sont tous deux
 *     des attributs de scene) avec le caractere '.'.
 * </p>
 *
 * @param scene structure sur laquel le tableau se verra initialiser.
 * @return 
 */
void init_scene(struct Scene * const scene);

/**
 * <p>va afficher tous les chaines de caracteres du tableau multidimensionnelle (*scene).scene de [0]-[row-1]</p>
 *
 * @param scene la struct scene duquelle son tableau sera afficher
 * @return
 */
void print_scene(const struct Scene * const scene);

/**
 * <p>Charge un rectangle dans le tableau multidimensionnelle de *scene selon les specification de la struct *rect.</p>
 *
 * <p>A noter que les valeur de rect.row et col peuvent etre negative</p>
 *
 * <p>Exemple : rect = {row = 1 , col = 2 , height = 2 , width = 5 , symbol = 'H'} </p>
 *
 * <p>Le tableau de *scene serait ensuite represente comme cela.</p>
 *
 * <p>...........</p>
 * <p>..HHHHH....</p> 
 * <p>..HHHHH....</p>
 *
 * @param scene la scene duquelle le tableau se verra modifier.
 * @param rect struct qui contient tous les specification necessaire charger le rectrangle.
 * @return
 */
void draw_rectangle(struct Scene * const scene, struct Rectangle * const rect);

/**
 * <p>Charge une croix dans le tableau multidimensionnelle de *scene selon les specification de la struct *plus</p>
 *
 * <p>A noter que les valeurs de plus.row et col peuvent etre negative</p>
 *
 * <p>Exemple : plus = {row = 1 , col = 5 , symbol = '+'}</p>
 *
 * <p>.....+.....</p>
 * <p>+++++++++++</p> 
 * <p>.....+....</p>
 *
 * @param scene la scene duquelle le tableau se verra modifier.
 * @param plus struct qui contient tous les specification necessaire charger la croix.
 * @return
 */
void draw_plus(struct Scene * const scene, struct Plus * const plus);

/**
 * <p>Charge un disque dans le tableau multidimensionnelle de *scene selon les specification de la struct *disk</p>
 *
 * <p>A noter que les valeurs de disk.row et col peuvent etre negative</p>
 *
 * <p>Exemple : plus = {row = 8 , col = 18 , rayon = 3 ,symbol = 'W'}</p>
 *
 * <p>............................</p>
 * <p>............................</p>
 * <p>............................</p>
 * <p>............................</p>
 * <p>............................</p>
 * <p>..................W.........</p>
 * <p>................WWWWW.......</p>
 * <p>................WWWWW.......</p>
 * <p>...............WWWWWWW......</p>
 * <p>................WWWWW.......</p>
 * <p>................WWWWW.......</p>
 * <p>..................W.........</p>

 *
 * @param scene la scene duquelle le tableau se verra modifier.
 * @param disk struct qui contient tous les specification necessaire charger le disk.
 * @return
 */
void draw_disk(struct Scene * const scene, struct Disk * const disk);


/**
 * <p>
 *     Modifie buffer avec la premiere sequence rencontrer dans STDIN. Si aucune sequence n'est rencontrer et que EOF est rencontrer alors la fonction retourne EOF.
 *     Si par contre il y a une sequence , la fonction retourne 1. Si une sequence est rencontrer mais quelle contient trop de caractere pour le buffer, le nombre 
 *     de caracteres pouvant etre inserer dans buffer y sera inserer mais la methode retournera 2.
 * </p>
 *
 * <p>Sequence : ensemble de caractere n'etant pas separe par un ou des espaces. (le caractere \n est considerer comme une sequence a part entiere.)</p>
 *
 * <p>Exemple d'appel succesive de next() : </p>
 * 
 * <p>
 *     STDIN = "hello world!\n"<br>
 *             "How are you ?"
 * </p>
 *
 * <p>sequence rencontrer : (pour chaques sequences la fonction retourne 1)</p>
 *
 * <ul>
 *  <li>hello</li>
 *  <li>world!</li>
 *  <li>\n</li>
 *  <li>How</li>
 *  <li>are</li>
 *  <li>you</li>
 *  <li>?</li> 
 * </ul>
 *
 * <p>Ensuite EOF est rencontrer donc buffer n'est pas modifier et la fonction retourne EOF</p>
 *
 * @param buffer tableau dans lequel la sequence sera copie
 * @return EOF ou 1 ou 2
 */
signed int next(char * const buffer);


/**
 * <p>Change tous les caracteres de buffer qui ne sont pas egal a \0 a \0.</p>
 *
 * @param buffer pointeur sur le premier caractere d'une chaine.
 * @return
 */
void clear(char * const buffer);

/**
 * <p>
 *     Cette methode va appeler succesivement les methodes necessaire pour le chargement d'un rectangle dans le
 *     tableau multidimensionnelle de *scene. Si les entrer de *rect sont valide selon les specification de la methode
 *     rect_valid() la methode va charger le rectangle dans *scene sinon un message d'erreur ainsi qu'un code d'erreur
 *     est affecter au champs respectif de *error.
 * </p>
 *
 * @param buffer chaine de caracteres analyser durant la fontion. 
 * @param scene struct Scene dans laquelle le rectangle sera charger si tous ce pase bien.
 * @param rect struct Rectangle qui se vera affecter une valeur respectif a chacun de ses champs si tous les entrers de buffer sont valides.
 * @param error struct Error qui se vera affecter une valeur si buffer contient des entrers invalides.
 * @return
 */
void rect_action(char * const buffer, struct Scene * const scene, struct Rectangle * const rect , struct Error * const error);

/**
 * <p>
 *     Cette methode va appeler succesivement les methodes necessaire pour le chargement d'un plus dans le
 *     tableau multidimensionnelle de *scene. Si les entrer de *plus sont valides selon les specifications de la methode
 *     plus_valid() la methode va charger le plus dans *scene sinon un message d'erreur ainsi qu'un code d'erreur
 *     est affecter au champs respectif de *error.
 * </p>
 *
 * @param buffer chaine de caracteres analyser durant la fontion. 
 * @param scene struct Scene dans laquelle le rectangle sera charger si tous ce pase bien.
 * @param plus struct Plus qui se vera affecter une valeur respectif a chacun de ses champs si tous les entrers de buffer sont valides.
 * @param error struct Error qui se vera affecter une valeur si buffer contient des entrers invalides.
 * @return
 */
void plus_action(char * const buffer, struct Scene * const scene , struct Plus * const plus ,struct Error * const error);

/**
 * <p>
 *     Cette methode va appeler succesivement les methodes necessaire pour le chargement d'un disk dans le
 *     tableau multidimensionnelle de *scene. Si les entrer de *disk sont valide selon les specification de la methode
 *     disk_valid() la methode va charger le disk dans *scene sinon un message d'erreur ainsi qu'un code d'erreur
 *     est affecter au champs respectif de *error.
 * </p>
 *
 * @param buffer chaine de caracteres analyser durant la fontion. 
 * @param scene struct Scene dans laquelle le disk sera charger si tous ce pase bien.
 * @param rect struct disk qui se vera affecter une valeur respectif a chacun de ses champs si tous les entrers de buffer sont valides.
 * @param error struct Error qui se vera affecter une valeur si buffer contient des entrers invalides.
 * @return
 */
void disk_action(char * const buffer , struct Scene * const scene , struct Disk * const disk , struct Error * const error);

/**
 * <p>
 *     Lit la premiere ligne de STDIN qui n'est pas composer uniquement d'espace et/ou de \n.
 *     Si cette ligne respecte le format <(int)1-20> <(int)1-40> sans se soucier du nombre d'espaces qui les separes
 *     la methode retourne 1 sinon 0.
 * </p>
 *
 * <p> Exemple : </p>
 *
 * <p>
 *      STDIN : "            \n"<br>
 *              "    12        28        \n"
 * </p>
 *
 * <p>La premiere ligne est sauter car elle est composer uniquement d'espace et d'un \n a la fin.</p>
 * <p>ensuite chacune des sequence respecte dans l'ordre le format presente ci-haut donc la fonction retourne 1</p>
 *
 * @param buffer tableau utiliser pour stocker les sequences de la premiere ligne les unes apres les autres.
 * @param scene la struct Scene dans laquelle on affectera les sequences valides..
 * @param error la struct Error dans laquelle sera affecter le message et le code d'erreur approprie en cas de sequence invalide.
 * @return un int ayant une valeur de 1 si valid 0 sinon.
 */
unsigned int first_line_valid(char * const buffer, struct Scene * const scene , struct Error * const error);

/**
 * <p>
 *     Modifie buffer avec la prochaine sequence de STDIN avec la methode next(). Si la sequence != \n la fonction retourne 1.
 *     Sinon la fonction retourne 0 et \n est remit dans STDIN.
 * </p>
 *
 * <p>Exemple : (Appel succesive de same_line() jusqu'au retour de 0)</p>
 *
 * <p> 
 *      STDIN = "hello world!\n"<br>
 *              "Salut tous le monde!"
 * </p>
 *
 * <p>Representation de buffer apres chaque appel de same_line().</p>
 *
 * <ul>
 *     <li>hello</li> retourn 1
 *     <li>world!</li> retourne 1
 *     <li>\n</li> retourne 0
 * </ul>
 *
 * @param buffer chaine dans laquelle la sequence sera copier.
 * @return 1 si apres l'appel *buffer != \n. 0 sinon.
 */
unsigned int same_line(char * const buffer);

/**
 * <p>
 *     Teste si la sequence de buffer == \n si c'est le cas la methode renvoie 1 et le char \n est renvoyer dans STDIN.
 *     Sinon renvoie 0
 * </p>
 *
 * @param buffer chaine de caractere representent la sequence.
 * @return un int avec la valeur 1 si la sequence == \n 0 sinon.
 */
unsigned int undo_endl(const char buffer);

/**
 * <p>
 *   Saute toute les lignes compose uniquement d'espaces et/ou de \n 
 *   et positione buffer sur la premiere sequence rencontrer qui != \n.
 * </p>
 *
 * <p>Exemple : </p>
 *
 * <p>
 *     STDIN = "         \n"<br>
 *             "\n"<br>
 *             "       \n"
 *             " hello world!  " 
 * </p>
 *
 * <p>Apres un appel de to_next_line() la sequence de buffer sera 'hello'.</p>
 *
 * @param buffer pointeur sur le premire char d'une chaine de caractere qui represente la sequence. 
 * @return 1 si une ligne non vide a ete trouver 0 si EOF est rencontrer.
 */
unsigned int to_next_line(char * const buffer);

/**
 * <p>Retourne 1 si *buffer == \n 0 sinon.</p>
 *
 * @param buffer un pointeur sur le premire caractere de la sequence
 * @return un int avec la valeur 1 si *buffer == \n 0 sinon.
 */
unsigned int is_endl(const char buffer);  // avant etais (const char * const buffer)

/**
 * <p>Renvoi 1 si les valeurs du buffer jusqu'au prochain caractere \n son valide selon les specifications suivante :</p>
 *
 * <p>
 *     r <i> <j> <h> <w> <c>, où <i> et <j> indiquent la ligne et la colonne
 *     auxquelles se trouve le coin supérieur gauche du rectangle, et <h> et
 *     <w> indiquent la hauteur et la largeur du rectangle. À noter que <i> et
 *     <j> peuvent être des entiers négatifs, mais pas <h> ni <w>. Le
 *     paramètre <c> est le caractère utilisé pour "remplir" le rectangle.
 * </p>
 *
 * <p>les valeurs de la struct *rect son modifier avec leur valeur respective si et seulement si la methode renvoie 1</p>
 *
 * <p>Exemple : </p>
 *
 * <p>
 *     STDIN = "   r      3 5    2     5   H   \n"
 * </p>
 *
 * <p>
 *    Cette sequence est valide car les espaces sont ignorer. Donc nous auront respectivement les sequences {3,5,2,5,h}. Le 'r' est sequencer avant l'appelle de
 *    cette fonction.   
 * </p>
 *
 * @param rect structure Rectangle dans laquelle la valeur de chaques sequences de buffer sera copie si la sequence est valide.
 * @param buffer pointeur sur le premier caratcere d'une chaine dans laquelle les sequences seront copie.
 * @return un int ayant comme valeur 1 si le rectangle est valide 0 sinon.
 */
unsigned int rect_valid(struct Rectangle * const rect, char * const buffer);

/**
 * <p>Renvoi 1 si les valeurs du buffer jusqu'au prochain caractere \n son valide selon les specifications suivante :</p>
 *
 * <p>
 *     + <i> <j> <c>, où <i> et <j> indiquent les indices de la ligne et de
 *     la colonne qui déterminent la croix et <c> est le caractère utilisé.
 *     <i> et <j> peuvent etre negatif.
 * </p>
 *
 * <p>les valeurs de la struct *plus son modifier avec leur valeur respective si et seulement si la methode renvoie 1</p>
 *
 * <p>Exemple : </p>
 *
 * <p>
 *     STDIN = "   +   3   3     T  \n"
 * </p>
 *
 * <p>
 *    Cette sequence est valide car les espaces sont ignorer. Donc nous auront respectivement les sequences {3,3,T}. Le '+' est sequencer avant l'appelle de
 *    cette fonction.   
 * </p>
 *
 * @param plus structure Plus dans laquelle la valeur de chaques sequences de buffer sera copie si la sequence est valide.
 * @param buffer pointeur sur le premier caratcere d'une chaine dans laquelle les sequences seront copie.
 * @return un int ayant comme valeur 1 si le plus est valide 0 sinon.
 */
unsigned int plus_valid(struct Plus * const plus, char * const buffer);

/**
 * <p>Renvoi 1 si les valeurs du buffer jusqu'au prochain caractere \n son valide selon les specifications suivante :</p>
 *
 * <p>
 *     d <i> <j> <r> <c>, où <i> et <j> indiquent les indices de la ligne et
 *     de la colonne où se situe le centre du disque, <r> est le "rayon" du
 *     disque et <c> le caractère à utiliser pour "remplir" le disque. Noter que
 *     le paramètre <r> doit être positif, sinon un message d'erreur doit être
 *     affiché.<i> et <j> peuvent etre negatif.
 * </p>
 *
 * <p>les valeurs de la struct *disk son modifier avec leur valeur respective si et seulement si la methode renvoie 1</p>
 *
 * <p>Exemple : </p>
 *
 * <p>
 *     STDIN = "   d   3    4   3   D   \n"
 * </p>
 *
 * <p>
 *    Cette sequence est valide car les espaces sont ignorer. Donc nous auront respectivement les sequences {3,4,3,D}. Le 'd' est sequencer avant l'appelle de
 *    cette fonction.   
 * </p>
 *
 * @param disk structure Disk dans laquelle la valeur de chaques sequences de buffer sera copie si la sequence est valide.
 * @param buffer pointeur sur le premier caratcere d'une chaine dans laquelle les sequences seront copie.
 * @return un int ayant comme valeur 1 si le disk est valide 0 sinon.
 */
unsigned int disk_valid(struct Disk * const disk , char * const buffer);

/**
 * <p>Change les valeur de la struct *error avec le message et le code d'erreur passer en parametre.</p>
 *
 * @param error la structure sur laquelle les valeurs seront change
 * @param message le message qui sera a affecte a l'attribut msgerr
 * @param err_code le code d'erreur qui sera affecte a l'attribut err_code
 * @return
 */
void set_error(struct Error * const error,const char * const message , const int err_code);

/**
 * <p>Copie le contenue de *s1 et *s2 dans *copy un a la suite de l'autre.*s1 est d'abord copie et ensuite *s2</p>
 *
 * <p>Exemple : </p>
 * 
 * <ul>
 *     <li>s1 = "hello "</li>
 *     <li>s2 = "world!."</li>
 *     <li>copy = [100]</li>
 * </ul>
 *
 * <p>Apres l'appel de append() la chaine de caracteres copy = "hello world!.\0"</p>
 *
 * @return
 */
void append(const char * const s1 , const char * const s2 , char * const copy);

/**
 * <p>
 *    Insere dans la chaine err le contenue de la chaine ERR_TYPE_OBJECT ainsi que buffer.
 *    Ajoute aussi a la fin de err <'.\0>.La methode set_error() est aussi appele a la fin.
 *    De ce fait a al fin de cette methode les variable err_code et err_msg de la struct error
 *    sont modifie.
 * </p>
 *
 * @param error Struct error de la quel les variables err_code et err_msg seront modifie
 * @param buffer Pointeur sur le premier char d'une chaine de charactere qui contient la sequence
 * @param err Tableau de chars dans lequelle les donnees manquante seront insere.
 * @return 
 */
void not_recognizable_type(struct Error *error,char *buffer , char *err );


/**
 * <p>fonction global qui execute toute les fonctions necessaire au bon fonctionnement du programme.</p>
 *
 * @return
 */
void execute(struct Scene *scene ,
             struct Error *error ,
             struct Rectangle *rect ,
             struct Plus *plus ,
             struct Disk *disk ,
             char *buffer ,
             char *err);



int main(int argc , char *argv[]){
    struct Scene scene;
    struct Error error;
    struct Rectangle rect;
    struct Plus plus;
    struct Disk disk;
    char buffer[BUFFER_SIZE];
    char err[ERR_BUFFER_SIZE];

    execute(&scene,&error,&rect,&plus,&disk,buffer,err);

    return error.err_code;
}


void execute(struct Scene * const scene ,
             struct Error *error ,
             struct Rectangle *rect ,
             struct Plus *plus ,
             struct Disk *disk ,
             char *buffer,
             char *err ){


  if(first_line_valid(buffer,scene,error)){
        unsigned int len = 0;

        init_scene(scene);
        
        while(!((*error).err_code) && next(buffer) != EOF){

            if(*buffer == '\n'){

                if(to_next_line(buffer)){

                    len = strlen(buffer);

                    if(*buffer == TYPE_R && len == 1){
                        rect_action(buffer,scene,rect,error);
                    }else if(*buffer == TYPE_P && len == 1){
                        plus_action(buffer,scene,plus,error);
                    }else if(*buffer == TYPE_D && len == 1){
                        disk_action(buffer,scene,disk,error);
                    }else{
                        not_recognizable_type(error,buffer,err);
                    }
                 
                }
            }
            clear(buffer);
        }
    }

    if(!(*error).err_code){
        print_scene(scene);
    }else{
        printf("%s",(*error).msgerr);
    }  
}

void not_recognizable_type(struct Error *error,char *buffer , char *err ){
    unsigned int start = strlen(buffer);
    unsigned int tmp;

    if(start >= ERR_BUFFER_SIZE - (tmp = strlen(ERR_TYPE_OBJECT))){
        start -= start - (ERR_BUFFER_SIZE - tmp - NB_CHARS_ADD_ERR); 
    }

    *(buffer + start) = '\'';
    *(buffer + start + 1) = '.';
    *(buffer + start + 2) = '\0';
    append(ERR_TYPE_OBJECT,buffer,err);
    set_error(error,err,ERR_INVALID_OBJECT);
}



void append(const char * const s1 , const char * const s2 , char * const copy){
    size_t i = 0;
    size_t j = 0;

    while(*(s1+i)){
        *(copy + (j++)) = *(s1 + (i++));
    }
    
    i = 0;

    while(*(s2 + i)){
        *(copy + (j++)) = *(s2 + (i++));
    }

    *(copy + j) = '\0';
}


unsigned int is_endl(const char buffer){
    return buffer == '\n' ? 1 : 0;
}

unsigned int to_next_line(char * const buffer){
    signed int is_not_eof;
    while((is_not_eof = next(buffer)) != EOF && *buffer == '\n');
    return is_not_eof == EOF ? 0 : 1;
}

void plus_action(char *buffer, struct Scene *scene, struct Plus *plus , struct Error *error){

    if(plus_valid(plus,buffer)){
        draw_plus(scene,plus);
    }else{
        set_error(error,ERR_PLUS,ERR_INVALID_OBJECT);
    }

}

void rect_action(char * const buffer, struct Scene * const scene, struct Rectangle * const rect , struct Error * const error){
    if(rect_valid(rect,buffer)){
        draw_rectangle(scene,rect);
    }else{
        set_error(error,ERR_RECTANGLE,ERR_INVALID_OBJECT);
    }
}

void disk_action(char * const buffer, struct Scene * const scene , struct Disk * const disk , struct Error * const error){
    if(disk_valid(disk,buffer)){
        draw_disk(scene,disk);
    }else{
        set_error(error,ERR_DISK,ERR_INVALID_OBJECT);
    }
}

void set_error(struct Error * const error,const char * const message , const int err_code){
    (*error).msgerr = message;
    (*error).err_code = err_code;
}


unsigned int first_line_valid(char * const buffer, struct Scene * const scene , struct Error * const error){
    
    unsigned int int_token;
    unsigned int nb_token = 0;

    if(to_next_line(buffer)){

        do{
            nb_token++;
            
            if(nb_token > MAX_TOKEN_FIRST_LINE || !is_int(buffer)){
                set_error(error,ERR_FL_DIM,ERR_INVALID_DIMENSION);
                return 0;
            }else if(nb_token == 1 && ((int_token = to_int(buffer)) <  MIN_ROW || int_token > MAX_ROW)){
                set_error(error,ERR_FL_ROW,ERR_INVALID_DIMENSION);
                return 0;
            }else if((int_token = to_int(buffer)) <  MIN_COL || int_token >=  MAX_COL){
                set_error(error,ERR_FL_COL,ERR_INVALID_DIMENSION);
                return 0;
            }

            if(nb_token == 1){
                (*scene).rows = to_int(buffer);
            }else{
                (*scene).cols = to_int(buffer);
            }
                
        }while(same_line(buffer));
    }else{
        set_error(error,ERR_FL_DIM,ERR_INVALID_DIMENSION);
        return 0;
    }

    return 1;
}

unsigned int plus_valid(struct Plus * const plus , char * const buffer){
    unsigned int nb_token = 1;

    while(same_line(buffer)){
        nb_token++;

        if(nb_token > MAX_TOKEN_P ||
          (nb_token < MAX_TOKEN_P && !is_int(buffer)) ||
          (nb_token == MAX_TOKEN_P && strlen(buffer) > 1)){
            return 0;
        }

        if(nb_token == ROW){
            (*plus).row = to_int(buffer);
        }else if(nb_token == COL){
            (*plus).col = to_int(buffer);
        }else{
            (*plus).symbol = *buffer;
        }
    }

    return nb_token == MAX_TOKEN_P ? 1 : 0;
}

unsigned int disk_valid(struct Disk * const disk , char * const buffer){

    unsigned int nb_token = 1;
    signed int tmp = 1;

    while(same_line(buffer)){
        nb_token++;

        if(nb_token > MAX_TOKEN_D ||
          (nb_token < MAX_TOKEN_D && !is_int(buffer)) ||
          ((tmp = to_int(buffer)) < 0 && nb_token == D_TOKEN_NEG) ||
          (nb_token == MAX_TOKEN_D && strlen(buffer) > 1)){
            return 0;
        }

        if(nb_token == ROW){
            (*disk).row = tmp;
        }else if(nb_token == COL){
            (*disk).col = tmp;
        }else if(nb_token == RAYON){
            (*disk).rayon = tmp;
        }else{
            (*disk).symbol = *buffer;
        }
    }

    return nb_token == MAX_TOKEN_D ? 1 : 0;
}

unsigned int rect_valid(struct Rectangle * const rect , char * const buffer){
    unsigned int nb_token = 1;
    signed int tmp;

    while(same_line(buffer)){
        nb_token++;

        if(nb_token > MAX_TOKEN_R ||
          (nb_token < MAX_TOKEN_R && !is_int(buffer)) ||
           ((nb_token < MAX_TOKEN_R && (tmp = to_int(buffer)) < 0) && nb_token > R_TOKEN_NEG) ||
           (nb_token == MAX_TOKEN_R && strlen(buffer) > 1)){
            return 0;
        }

        if(nb_token == ROW){
            (*rect).row = tmp;
        }else if(nb_token == COL){
            (*rect).col = tmp;
        }else if(nb_token == HEIGHT){
            (*rect).height = tmp;
        }else if(nb_token == WIDTH){
            (*rect).width = tmp;
        }else{
            (*rect).symbol = *buffer;
        }
    }

   return nb_token == MAX_TOKEN_R ? 1 : 0;
}


unsigned int same_line(char * const buffer){
    if(next(buffer) != EOF && !undo_endl(*buffer)){
        return 1;
    }

    return 0;
}

unsigned int undo_endl(const char buffer){
    if(buffer == '\n'){
        ungetc('\n',stdin);
        return 1;
    }

    return 0;
}

void draw_disk(struct Scene * const scene, struct Disk * const disk){
    signed int start_row  = (*disk).row - (*disk).rayon + 1;
    signed int end_row = (*disk).row + (*disk).rayon - 1;
    signed int start_col = (*disk).col - (*disk).rayon + 1;
    signed int end_col = (*disk).col + (*disk).rayon - 1;
    signed int tmp;
    unsigned int ybound = ((*disk).col > -1 && (*disk).col < (*scene).cols) ? 1 : 0; 
    unsigned int xbound = ((*disk).row > -1 && (*disk).row < (*scene).rows) ? 1 : 0;

    if(start_row < 0){
        start_row = 0;
    }
    
    if(start_col < 0){
        start_col = 0;
    }

    tmp = start_col;
    
    if(start_row > 0  && ybound){
        *(*((*scene).scene + start_row - 1) + (*disk).col) = (*disk).symbol;
    }

    if(start_col > 0 && xbound){
        *(*((*scene).scene + (*disk).row) + start_col - 1) = (*disk).symbol;   
    }

    if(end_col < (*scene).cols && xbound){
        *(*((*scene).scene + (*disk).row) + end_col + 1) = (*disk).symbol;
    }

    if(end_row < (*scene).rows && ybound){
        *(*((*scene).scene + end_row + 1) + (*disk).col) = (*disk).symbol;
    }

    while(start_row <= end_row && start_row < (*scene).rows){
        start_col = tmp;

        while(start_col <= end_col && start_col < (*scene).cols){

            if(pow(start_row-(*disk).row,EXP) + pow(start_col-(*disk).col,EXP) <= pow((*disk).rayon,EXP)){
                 *(*((*scene).scene + start_row) + start_col) = (*disk).symbol;
            }

            start_col++;
        }

        start_row++;
    }
    
}


void draw_plus(struct Scene * const scene, struct Plus * const plus){
    
    size_t i = 0;

    if((*plus).row >= 0 && (*plus).row < (*scene).rows){

        while(*(*((*scene).scene + (*plus).row ) + i)){
            *(*((*scene).scene + (*plus).row) + i) = (*plus).symbol;
            i++;
        }
    }

    i = 0;

    if((*plus).col >= 0 && (*plus).col < (*scene).cols){

        while(i < (*scene).rows ){
            *(*((*scene).scene + i) + (*plus).col) = (*plus).symbol;
            i++;
        }
    }
}


void draw_rectangle(struct Scene * const scene, struct Rectangle * const rect){
    signed int nbrow = (*rect).row + (*rect).height; 
    signed int nbcol = (*rect).col + (*rect).width;
    signed int tmp = (*rect).col;

    if((*rect).col < 0){
        tmp = 0;
    }else if(nbcol > (*scene).cols){
        nbcol = (*scene).cols; 
    }
    
    if((*rect).row < 0){
        (*rect).row = 0;
    }else if(nbrow > (*scene).rows){
        nbrow = (*scene).rows;
    }

    while((*rect).row < nbrow){
        (*rect).col = tmp;

        while((*rect).col < nbcol){
            *(*((*scene).scene + (*rect).row) + (*rect).col++ ) = (*rect).symbol;
        }

        (*rect).row++;
    }
}


void print_scene(const struct Scene * const scene){
    size_t i = 0;

    while(i < (*scene).rows){
        printf("%s\n", *((*scene).scene + i));
        i++;
    }
}

void init_scene(struct Scene * const scene){
    size_t i = 0;
    size_t j = 0;

    while(i < (*scene).rows){
        j = 0;

        while(j < (*scene).cols){
            *(*((*scene).scene + i) + (j++)) = '.';
        }

        *(*((*scene).scene + i) + j) = '\0';
        i++;
    }
}


unsigned int is_int(const char * const str){
    size_t i = 0;

    if((*str == '-' || *str == '+') && *(str + 1) && *(str + 1) > '0'){
        i++;
    }

    while(*(str + i)){

        if(*(str + i) < '0' || *(str + i) > '9'){
            return 0;
        }

        i++;
    }

    return 1;
}

signed int to_int(const char * const str){
    unsigned int power = pow(10,len_int(str) - 1);
    signed int nb = 0;
    size_t i = 0;
    unsigned int multiplier = 1;

    if(*str == '-'){
        i++;
        multiplier = -1;
    }else if(*str == '+'){
        i++;
    }

    while(*(str + i)){
        nb += (*(str + (i++)) - '0') * power;
        power /= 10;
    }

    return nb * multiplier;
}

unsigned int len_int(const char * const str){
    unsigned int nb = 0;
    size_t i = 0;

    if(*str == '-' || *str == '+'){
        i++;
    }

    while(*(str + (i++))){
        nb++;
    }

    return nb;
}


signed int next(char * const buffer){
    size_t i = 0;
    signed int c;
   
    while((c = getchar()) == ' ');

    if(c != EOF){

        *(buffer + (i++)) = c;

        if(c != '\n'){

            while((c = getchar()) != ' ' && c != '\n'  && c != EOF){

                if(i >= BUFFER_SIZE){
                   return 2;
                }

                *(buffer + (i++)) = c;
            }

            undo_endl(c);
        }

        *(buffer + i) = '\0';

        return 1;
    }

    return EOF;
}

void clear(char * const buffer){
    size_t i = 0;

    while(*(buffer + i)){
        *(buffer + (i++)) = '\0';
    }
}
